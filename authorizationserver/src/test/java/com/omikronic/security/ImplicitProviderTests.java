package com.omikronic.security;

import org.junit.BeforeClass;
import org.springframework.boot.test.SpringApplicationConfiguration;

import sparklr.common.AbstractImplicitProviderTests;

/**
 * @author Dave Syer
 */
@SpringApplicationConfiguration(classes=AuthorizationServerApplication.class)
public class ImplicitProviderTests extends AbstractImplicitProviderTests {

	@BeforeClass
	public static void setup() {
		System.setProperty("spring.config.name", "authserver");
	}

}
