package com.omikronic.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableGlobalMethodSecurity(proxyTargetClass=true)
public class AuthorizationServerApplication {

	 public static void main(String[] args) {
		 System.setProperty("spring.config.name", "authserver");
	     SpringApplication.run(AuthorizationServerApplication.class, args);
	 }
}