package com.omikronic.security;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.builders.InMemoryClientDetailsServiceBuilder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import com.omikronic.security.AuthorizationProperties.ClientDetails;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {
	
	@Autowired
	AuthorizationProperties props;
	
	@Autowired(required=false)
	DataSource datasource;
	
    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;
	
    @Autowired
    @Qualifier("ldapUserDetailsService")
    private UserDetailsService userDetailsService;
    
    @Autowired
    private JwtAccessTokenConverter accessTokenConverter;
    
    @Override
    public void configure(final AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
    	oauthServer
    		.tokenKeyAccess(props.getTokenKeyAccessGrants())
    		.checkTokenAccess(props.getCheckTokenAccessGrants());
    }

    @Override
    public void configure(final ClientDetailsServiceConfigurer clients) throws Exception {
        List<ClientDetails> clientDetails = props.getClientDetails();
        if (clientDetails != null && clientDetails.size() >0) { 
	    	InMemoryClientDetailsServiceBuilder inMemory = clients.inMemory();
	        for (ClientDetails client : clientDetails) {
	        	inMemory.withClient(client.getClientId())
	        			.secret(client.getClientSecret())
	        			.authorizedGrantTypes(client.getAuthorizedGrantTypes())
	        			.authorities(client.getAuthorities())
	        			.scopes(client.getScope())
	        			.accessTokenValiditySeconds(client.getAccessTokenValiditySeconds())
	        			.refreshTokenValiditySeconds(client.getRefreshTokenValiditySeconds())
	        			.redirectUris(client.getRegisteredRedirectUri())
	        			.additionalInformation(client.getAdditionalInformation())
	        			.resourceIds(client.getResourceIds())
	        			.autoApprove(client.getAutoApproveScopes());
	        }
        }
        else {
        	clients.jdbc(datasource);
        }
    }
    
    
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authenticationManager(authenticationManager)
        			.accessTokenConverter(accessTokenConverter)
        			.userDetailsService(userDetailsService);
    }
}
