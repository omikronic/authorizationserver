package com.omikronic.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;

@Configuration
public class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {

	@Autowired
	AuthorizationProperties props;
	
    @Override
    public void init(final AuthenticationManagerBuilder auth) throws Exception {
		auth
			.ldapAuthentication()
				.userSearchBase(props.getLdapAuthentication().getUserSearchBase())
				.userSearchFilter(props.getLdapAuthentication().getUserSearchFilter())
				.userDnPatterns(props.getLdapAuthentication().getUserDnPatterns())
				.groupSearchBase(props.getLdapAuthentication().getGroupSearchBase())
				.groupSearchFilter(props.getLdapAuthentication().getGroupSearchFilter())
				.groupRoleAttribute(props.getLdapAuthentication().getGroupRoleAttribute())
				.rolePrefix(props.getLdapAuthentication().getRolePrefix())
			.contextSource()
				.port(props.getLdapAuthentication().getPort())
				.managerDn(props.getLdapAuthentication().getManagerDn())
				.managerPassword(props.getLdapAuthentication().getManagerPassword())
				.root(props.getLdapAuthentication().getRootDn())
				.url(props.getLdapAuthentication().getExternalUrl())
				.ldif(props.getLdapAuthentication().getLdifFile());
    }
}