# Omikronic Authorization Server #

The main purpose of this project is to have an easy configurable authorization server token based for our microservices. We have chosen the Spring Security OAuth2 for our implementation and we have configured an LDAP authentication provider for end-users authentication.

## Modules ##

### Authorization Server ###
This module contains the code and configuration for the Authorization Server.


### Dockerized Authorization Server ###
This module contains the Dockerfile and some files to customize how the Authorization Server must be configured for creating a Docker image.  

### Spring OAuth2 Tests Common ###
This module is a clone from the one with the same name that you can find at [Spring Github repository](https://github.com/spring-projects/spring-security-oauth/tree/master/tests/annotation/common). We use those tests for having some tests on our authorization server implementation. 

## Features Overview ##
* Authorization Server supports different grant types:
    - Authorization Code
    - Implicit
    - Password
    - Client Credentials
* End-Users can be configured to stay in an embedded LDAP server or an external one
* Client Details can be stored in database or in memory
* It uses JSON Web Token (JWT) to avoid storing tokens in the server

## Technologies used in this project ##
* Languages:
    - Java
* Spring:
    - Boot
    - Security OAuth2
    - Security LDAP
* Apache Directory Services (DS)
* REST
* Testing:
    - JUnit
* Lombok
* Git
