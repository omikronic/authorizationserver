package com.omikronic.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="authserver")
@Getter
@Setter
@ToString
public class AuthorizationProperties {
	String accessTokenSigningKey;
	String tokenKeyAccessGrants;
	String checkTokenAccessGrants;
	List<ClientDetails> clientDetails = new ArrayList<ClientDetails>();
	String[] allowedPaths = new String[0];
	String[] securizedPaths = new String[0];
	boolean csrfDisabled = true;
	LdapAuthentication ldapAuthentication = new LdapAuthentication();
	
	@Getter
	@Setter
	@ToString
	public static class ClientDetails {
		String clientId;
		String clientSecret;
		String[] authorizedGrantTypes;
		int accessTokenValiditySeconds = 60 * 60 * 12; // 12 hours
		int refreshTokenValiditySeconds = 60 * 60 * 24 * 30; // 30 days
		String[] registeredRedirectUri = new String[0];
		String[] scope = new String[0];
		String[] authorities = new String[0];
		String[] resourceIds = new String[0];
		String[] autoApproveScopes = new String[0];
		Map<String, String> additionalInformation = new HashMap<String, String>();
	}
	
	@Getter
	@Setter
	@ToString
	public static class LdapAuthentication {
		String userSearchBase;
		String userSearchFilter;
		String[] userDnPatterns = new String[0];
		String groupSearchBase;
		String groupSearchFilter;
		String groupRoleAttribute;
		String rolePrefix;
		
		int port;
		String managerDn;
		String managerPassword;
		String rootDn;
		String externalUrl;
		String ldifFile;
		String[] providerUrls = new String[0];
	}
}
