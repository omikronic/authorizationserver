/*
 *   Licensed to the Apache Software Foundation (ASF) under one
 *   or more contributor license agreements.  See the NOTICE file
 *   distributed with this work for additional information
 *   regarding copyright ownership.  The ASF licenses this file
 *   to you under the Apache License, Version 2.0 (the
 *   "License"); you may not use this file except in compliance
 *   with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 *
 */
package org.apache.directory.shared.ldap.schema.ldif.extractor.impl;


import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.CodeSource;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


public class ResourceMap
{
   /**
    * Get a Map of resources
    * Pattern pattern = Pattern.compile(".*").  The keys represent
    * resource names and the boolean parameter indicates whether or
    * not the resource is in a Jar file.
    * 
    * @param pattern the pattern to match
    * @return the resources with markers - true if resource is in Jar
    */
    public static Map<String,Boolean> getResources( Pattern pattern )
    {
        HashMap<String,Boolean> retval = new HashMap<String,Boolean>();
		CodeSource src = DefaultSchemaLdifExtractor.class.getProtectionDomain().getCodeSource();
		if (src != null) {
			URL jar = src.getLocation();
			
			try {
				URLConnection conn = jar.openConnection();
				if (conn instanceof JarURLConnection) {
					JarURLConnection connection = (JarURLConnection)conn;
					JarFile jarFile = connection.getJarFile();
					Enumeration<JarEntry> entries = jarFile.entries();
					while (entries.hasMoreElements()) {
						JarEntry entry = entries.nextElement();
						String name = entry.getName();
						if (pattern.matcher(name).matches()) {
							retval.put(name, Boolean.TRUE);
						}
					}
				}
				else {
					ZipInputStream zip;
					zip = new ZipInputStream(conn.getInputStream());
					while (true) {
						ZipEntry e = zip.getNextEntry();
						if (e == null)
							break;
						String name = e.getName();
						if (pattern.matcher(name).matches()) {
							retval.put(name, Boolean.TRUE);
						}
					}
				}
			} catch (IOException e) {
				System.out.println("Problem reading the JAR");
				e.printStackTrace();
			}
		}
        
        return retval;
    }
}