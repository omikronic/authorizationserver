package com.omikronic.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.security.ldap.search.LdapUserSearch;
import org.springframework.security.ldap.userdetails.LdapUserDetailsService;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	AuthorizationProperties props;

	@Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
    	http
    		.formLogin().permitAll()
    	.and()
    		.authorizeRequests()
    		.antMatchers(props.getAllowedPaths()).permitAll()
    		.antMatchers(props.getSecurizedPaths()).authenticated();
    	/*.and()
    		.httpBasic()
    		.realmName("OAuth Server");*/
    	if (props.isCsrfDisabled()) {
	        http
	        	.csrf()
	        	.disable();
    	}
    }

    @Bean
    public UserDetailsService ldapUserDetailsService() {
    	return new LdapUserDetailsService(ldapUserSearch());
    }
    
    @Bean
    public LdapUserSearch ldapUserSearch() {
    	return new FilterBasedLdapUserSearch(props.getLdapAuthentication().getUserSearchBase(), 
    										props.getLdapAuthentication().getUserSearchFilter(), 
    										ldapContextSource());
    }
    
    @Bean
    public BaseLdapPathContextSource ldapContextSource() {
    	return new DefaultSpringSecurityContextSource(Arrays.asList(props.getLdapAuthentication().getProviderUrls()), props.getLdapAuthentication().getRootDn());
    }
}