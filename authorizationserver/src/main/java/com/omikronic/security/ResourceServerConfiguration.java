package com.omikronic.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
	
	@Autowired
	AuthorizationProperties props;
	
    @Override
    public void configure(HttpSecurity http) throws Exception {
    	http
    		.formLogin().permitAll()
    	.and()
			.authorizeRequests()
			.antMatchers(props.getAllowedPaths()).permitAll()
			.antMatchers(props.getSecurizedPaths()).authenticated();
		/*.and()
			.httpBasic()
			.realmName("OAuth Server");*/
		if (props.isCsrfDisabled()) {
	        http
	        	.csrf()
	        	.disable();
		}
    }
}