package com.omikronic.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.aop.framework.Advised;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AuthorizationServerApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port=0")
public class AuthorizationServerTest {

	@Value("${local.server.port}")
	private int port;

	private DefaultTokenServices tokenServices;
	
	@Autowired
	@Qualifier("defaultAuthorizationServerTokenServices")
	private void setTokenServices(DefaultTokenServices tokenServices) throws Exception {
		this.tokenServices = (DefaultTokenServices)((Advised)tokenServices).getTargetSource().getTarget();
	}

	@BeforeClass
	public static void setup() {
		System.setProperty("spring.config.name", "authserver");
	}

	@Test
	public void tokenStoreIsJwt() throws Exception {
		assertTrue("Wrong token store type: " + tokenServices,
				ReflectionTestUtils.getField(tokenServices, "tokenStore") instanceof JwtTokenStore);
	}

	@Test
	public void tokenKeyEndpointProtected() {
		assertEquals(HttpStatus.UNAUTHORIZED,
				new TestRestTemplate().getForEntity("http://localhost:" + port + "/oauth/token_key", String.class)
						.getStatusCode());
	}

	@Test
	public void tokenKeyEndpointWithSecret() {
		assertEquals(
				HttpStatus.OK,
				new TestRestTemplate("acme", "acmesecret").getForEntity(
						"http://localhost:" + port + "/oauth/token_key", String.class).getStatusCode());
	}

}


/*import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={AuthorizationServerApplication.class}, loader = SpringApplicationContextLoader.class)
public class AuthorizationServerTest {
	@Test
	public void test() {
		
	}
}*/