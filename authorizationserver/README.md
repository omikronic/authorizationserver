# Authorization Server #

This module contains the code and configuration for the Authorization Server. It is based on the Spring Security OAuth2 project integrated with the Spring Security LDAP one.

When we got the minimum code for having the tests in green, we started moving the hardcoded configuration to a YAML properties file. And we also worked on the integration with Spring Security LDAP and the embedded Apache DS (see [LDAP Integration](#ldap-integration)).


## Configuration Properties ##

We use a YAML configuration file:

		server:
		  port: #Authorization Server Listening Port (8081)
		  contextPath: #Authorization Server Context Path (/uaa)
		security:
		  user:
		    name: #Default User (for testing purpose)
		    password: #Default Password (for testing purpose)
		management:
		  context_path: #Context Path for Spring Actuator (/admin)
		logging:
		  level: #You can configure what you want, but these two reduce the logging in the startup
		    org.apache.directory.shared.ldap.entry.StringValue: WARN
		    org.apache.directory.shared.ldap.schema.manager.impl.DefaultSchemaManager: WARN
		spring:
		  datasource:
		    schema: #DDL for setting up the client details table (classpath:/authserver-schema.sql)
		    data: #DML for setting up some preconfigured clients (classpath:/authserver-data.sql)
           #It is preconfigured for using an H2 in-memory DB
           #but adding other drivers, it can connect to a remote DB
		    url: jdbc:h2:mem:testdb2
		    username: sa
		    password: 
		    driver-class-name: org.h2.Driver  
		authserver:
		  accessTokenSigningKey: #A key for signing the JWT token that must be the same in both Authorization Server and Resource Server
		  tokenKeyAccessGrants: #Grants for accessing to the token key
		  checkTokenAccessGrants: #Grants for accessing to the token check 
		  allowedPaths: #List of (ant style) paths that are not securized
		    - /login
		  securizedPaths: #List of (ant style) paths that are securized 
		    - /**
		  csrfDisabled: #A way to enable/disable the CSRF check (true)
		  ldapAuthentication:
		    rootDn: #Root for LDAP branches (dc=omikronic,dc=com)
		    userDnPatterns: #Different patterns to identify users
		      - uid={0}
		    userSearchBase: #Branch for user searching (ou=people)+rootDn
		    userSearchFilter: #Filter for users ((uid={0}))
		    groupSearchBase: #Branch for group searching (ou=groups)+rootDn
		    groupSearchFilter: #Filter for groups ((uniqueMember={0}))
		    groupRoleAttribute: #Attribute for getting role names (cn)
		    rolePrefix: #Prefix to preppend to the groups (ROLE_)
		    port: #Port where the LDAP Server will be listening to (389)
		    managerDn: #Admin user for binding (if commented, LDAP Server allows anonymous access)
		    managerPassword: #Admin password for binding
		    externalUrl: #URL to connect to a external LDAP Server intead of using the embedded one (ldap://your_ldap_host:389/dc=omikronic,dc=com)
		    ldifFile: #LDIF file with some preconfigured users and groups (classpath:default-server.ldif)
		    providerUrls: #List of URLs for LDAP providers
		      - ldap://localhost:389

			clientDetails: #Instead of using the spring datasource for retrieving the Client Details
			               #you can create the clients you want and store them in memory
			    - clientId: #Client Name
			      clientSecret: #Client Password
			      accessTokenValiditySeconds: #TTL for the Access Token
			      refreshTokenValiditySeconds: #TTL for the Refresh Token
			      authorizedGrantTypes: #List of Authorized Grant Types
			         - 
			      scope: #List of Scopes
			         - 
			      authorities: #List of Authorities
			         - 
			      registeredRedirectUri: #List of Registered Redirect URIs
			         - 
			      resourceIds: #List of Resource IDs
			         - 
			      autoApproveScopes: #List of Auto Approved Scopes
			         - 
			      additionalInformation: #Additional information included in the token (still not working)
			         property: value     



## LDAP Integration ##

With the Apache DS version 1.5.5 (the one supported by Spring Security LDAP) we found a weird error:

		2016-05-10 13:35:43.418  WARN 12516 --- [pool-3-thread-1] o.a.d.server.ldap.LdapProtocolHandler    : Unexpected exception forcing session to close: sending disconnect notice to client.
		
		java.lang.NullPointerException: null
			at org.apache.directory.server.ldap.handlers.LdapRequestHandler.handleMessage(LdapRequestHandler.java:129)
			at org.apache.directory.server.ldap.handlers.LdapRequestHandler.handleMessage(LdapRequestHandler.java:56)
			at org.apache.mina.handler.demux.DemuxingIoHandler.messageReceived(DemuxingIoHandler.java:232)
			at org.apache.directory.server.ldap.LdapProtocolHandler.messageReceived(LdapProtocolHandler.java:194)
			at org.apache.mina.core.filterchain.DefaultIoFilterChain$TailFilter.messageReceived(DefaultIoFilterChain.java:721)
			at org.apache.mina.core.filterchain.DefaultIoFilterChain.callNextMessageReceived(DefaultIoFilterChain.java:433)
			at org.apache.mina.core.filterchain.DefaultIoFilterChain.access$1200(DefaultIoFilterChain.java:47)
			at org.apache.mina.core.filterchain.DefaultIoFilterChain$EntryImpl$1.messageReceived(DefaultIoFilterChain.java:801)
			at org.apache.mina.core.filterchain.IoFilterEvent.fire(IoFilterEvent.java:71)
			at org.apache.mina.core.session.IoEvent.run(IoEvent.java:63)
			at org.apache.mina.filter.executor.UnorderedThreadPoolExecutor$Worker.runTask(UnorderedThreadPoolExecutor.java:480)
			at org.apache.mina.filter.executor.UnorderedThreadPoolExecutor$Worker.run(UnorderedThreadPoolExecutor.java:434)
			at java.lang.Thread.run(Thread.java:745)
		
		2016-05-10 13:35:43.419 DEBUG 12516 --- [o-auto-1-exec-7] .s.s.l.u.DefaultLdapAuthoritiesPopulator : Searching for roles for user 'xxx', DN = 'uid=xxx,ou=people,dc=springframework,dc=org', with filter (uniqueMember={0}) in search base 'ou=groups'
		2016-05-10 13:35:43.419  WARN 12516 --- [pool-3-thread-1] o.a.d.server.ldap.LdapProtocolHandler    : Null LdapSession given to cleanUpSession.

It seems that this error [was solved in the 1.5.6 version](https://issues.apache.org/jira/browse/DIRSERVER-1404) (or newer). So we started working on a way to enable newer versions of Apache DS with Spring Security LDAP. And that is why we have modified the [ApacheDSContainer class](https://bitbucket.org/omikronic/authorizationserver/diff/authorizationserver/src/main/java/org/springframework/security/ldap/server/ApacheDSContainer.java?diff2=aee2edfb13b3&at=master) to support the Apache DS 1.5.7 version (the last stable in this moment), the main changes are:
* Solve compilation errors due to the change on the naming of some classes
* Initialize the system partition programmatically
* Initialize some default LDAP schemas 

After doing this, we got all the tests in green again and we thought that it was all done. But it started to fail when we run the Authorization Server from the Spring Boot über JAR. The error was pointing us to a problem loading the default LDAP schemas in the Apache DS. We dig a bit in the code a we found the [ResourceMap class](https://bitbucket.org/omikronic/authorizationserver/src/c1562b839abb9de72ca28cbf9aeda7a68f13d594/authorizationserver/src/main/java/org/apache/directory/shared/ldap/schema/ldif/extractor/impl/ResourceMap.java?at=master&fileviewer=file-view-default) that was looking for resources in the "java.class.path" system property. That is not fine for über JAR releases, so [we changed it a bit](https://bitbucket.org/omikronic/authorizationserver/diff/authorizationserver/src/main/java/org/apache/directory/shared/ldap/schema/ldif/extractor/impl/ResourceMap.java?diff2=aee2edfb13b3&at=master) to allow the Authorization Server to start with a simple `java -jar authorizationserver.jar`.


## Tests ##

We haven't generated new tests yet, but we have taken those from the [Spring Github repository](https://github.com/spring-projects/spring-security-oauth/tree/master/tests/annotation/jwt) as a good starting point for validating our idea.