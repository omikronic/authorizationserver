# Dockerized Authorization Server #

This module allows you to create an Authorization Server Docker Image with the configuration files on `src/main/resources`.

Revise the configuration of your Docker installation in the pom.xml file and run:

`mvn clean install docker:build`

It will create a Docker Image (& Container) in your Docker Host and you will be able to manage it from your Docker console.

You could run it using:
`docker run --name authserver -p 8081:8081 omikronic/authserver`

Enjoy it.