insert into oauth_client_details values('acme', null, 'acmesecret', 'openid', 'authorization_code,refresh_token,password,client_credentials', null, 'ROLE_TRUSTED_CLIENT', 60*60*12, 60*60*24*30, null, null);
insert into oauth_client_details values('my-client-with-secret', null, 'secret', 'read,write', 'password,client_credentials', null, 'ROLE_CLIENT,ROLE_TRUSTED_CLIENT', 60*60*12, 60*60*24*30, null, null);
insert into oauth_client_details values('my-trusted-client', null, null, 'read,write,trust', 'authorization_code,refresh_token,password,implicit', null, 'ROLE_CLIENT,ROLE_TRUSTED_CLIENT', 60, 160, null, null);
insert into oauth_client_details values('my-client-with-registered-redirect', null, null, 'read,trust', 'authorization_code', 'http://anywhere?key=value', 'ROLE_TRUSTED_CLIENT', 60, 160, null, null);



